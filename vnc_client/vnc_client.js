// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-2015

// NPM packages
var program     = require('commander');           // parsing command-line arguments

// Local scripts
var vncClient   = require('./lib/node-vncclient');      // open a VNC connection
var websocketIO = require('./lib/node-websocket.io');   // creates WebSocket server and clients

/***************************************************************************************/

// Parse floating point pair (e.g. for pos)
var parseFPP = function (str) {
  var strs = str.split(',');
  console.log("strs "+strs);
  var ints = [parseFloat(strs[0]),parseFloat(strs[1])];
  console.log("ints "+ints);
  return ints;
}


// Command line arguments
program
  .version('0.0.1')
  .option('-s, --server <value>',      'server hostname (127.0.0.1)', '127.0.0.1')
  .option('-p, --port <n>',            'port number (5900)', 5900, parseInt)
  .option('-c, --catchphrase <value>', 'password (secret)', 'secret')
  .option('-d, --display <value>',     'SAGE url (wss://localhost:443)', 'wss://localhost:443')
  .option('-t, --title <value>',     '(extra) window title', '')
  .option('-o, --pos <value>',     '(extra) window pos', '[1.0, 0.0]', parseFPP)
  .option('-r, --resize <n>',     'resize to width', undefined, parseInt)
  .parse(process.argv);

program.ppos=[0.0,0.0];
if (program.pos!=null && program.pos!=undefined) {
  program.ppos=parseFPP(program.pos);
}

program.presize=1920;
if (program.resize!=null && program.resize!=undefined) {
  program.presize=parseInt(program.resize);
}

console.log("VNC> ", program.server, program.port, program.catchphrase, program.display, program.ppos, program.presize);
console.log("program args "+JSON.stringify(program));

var client = new vncClient(program.server, program.port, program.catchphrase);
var ws     = null;
var ready  = false;

var elemWidth  = 0;
var elemHeight = 0;
var buttons    = 0;
var ptrx = 0;
var ptry = 0;
var keypress    = false;
var isShift = false;
var isPressed = {};
var clipboardText = '';

//////////////////////////////////////////////////////////////////////////////////////
// From: https://github.com/kanaka/noVNC/blob/master/include/keyboard.js
//
// Given a keycode, try to predict which keysym it might be.
// If the keycode is unknown, null is returned.
function keysymFromKeyCode(keycode, shiftPressed) {
/*
    // IanP note: for now this function is designated for detection of special keys only
    // For "non-special" keys, return null
    if (typeof(keycode) !== 'number') {
        return null;
    }
    // won't be accurate for azerty
    if (keycode >= 0x30 && keycode <= 0x39) {
        return keycode; // digit
    }
    if (keycode >= 0x41 && keycode <= 0x5a) {
        // remap to lowercase unless shift is down
        return shiftPressed ? keycode : keycode + 32; // A-Z
    }
    if (keycode >= 0x60 && keycode <= 0x69) {
        return 0xffb0 + (keycode - 0x60); // numpad 0-9
    }

    switch(keycode) {
        case 0x20: return 0x20; // space
        case 0x6a: return 0xffaa; // multiply
        case 0x6b: return 0xffab; // add
        case 0x6c: return 0xffac; // separator
        case 0x6d: return 0xffad; // subtract
        case 0x6e: return 0xffae; // decimal
        case 0x6f: return 0xffaf; // divide
        case 0xbb: return 0x2b; // +
        case 0xbc: return 0x2c; // ,
        case 0xbd: return 0x2d; // -
        case 0xbe: return 0x2e; // .
    }
*/    
    return nonCharacterKey({keyCode: keycode});
}

// if the key is a known non-character key (any key which doesn't generate character data)
// return its keysym value. Otherwise return null
function nonCharacterKey(evt) {
    // evt.key not implemented yet
    if (!evt.keyCode) { return null; }
    var keycode = evt.keyCode;

    if (keycode >= 0x70 && keycode <= 0x87) {
        return 0xffbe + keycode - 0x70; // F1-F24
    }
    switch (keycode) {

        case 8  : return 0xFF08; // BACKSPACE
        case 13 : return 0xFF0D; // ENTER

        case 9  : return 0xFF09; // TAB

        case 27 : return 0xFF1B; // ESCAPE
        case 46 : return 0xFFFF; // DELETE

        case 36 : return 0xFF50; // HOME
        case 35 : return 0xFF57; // END
        case 33 : return 0xFF55; // PAGE_UP
        case 34 : return 0xFF56; // PAGE_DOWN
        case 45 : return 0xFF63; // INSERT

        case 37 : return 0xFF51; // LEFT
        case 38 : return 0xFF52; // UP
        case 39 : return 0xFF53; // RIGHT
        case 40 : return 0xFF54; // DOWN
        case 16 : return 0xFFE1; // SHIFT
        case 17 : return 0xFFE3; // CONTROL
        case 18 : return 0xFFE9; // Left ALT (Mac Option)

        case 224 : return 0xFE07; // Meta
        case 225 : return 0xFE03; // AltGr
        case 91  : return 0xFFEC; // Super_L (Win Key)
        case 92  : return 0xFFED; // Super_R (Win Key)
        case 93  : return 0xFF67; // Menu (Win Menu), Mac Command

	// HACK? let (CTRL-) c and v pass through
	case 67  : return 67 + 32; // c?
	case 86  : return 86 + 32; // v?

        default: return null;
    }
}
//////////////////////////////////////////////////////////////////////////////////////

var displayFrameRate = 10;
var clientFrameRate = 10;

function exitHandler(options, err) {
    console.log("exitHandler\n");
    if (options.cleanup) console.log('clean');
    if (err) console.log(err.stack);
    if (options.exit) {
      ws.emit('stopMediaBlockStream', {id: client.id});
      ws.ws.close();
      console.log("user exit\n");
      process.exit();
    }
}



function opencb() {
	elemHeight = client.conn.height;
	elemWidth  = client.conn.width;

	console.log("Got my connection", elemWidth, elemHeight);


	//var wsURL = "wss://" + "localhost" + ":" + "443";
	var wsURL = program.display;
	ws = new websocketIO(wsURL, false, function() {
		console.log("websocket open: ", ws.remoteAddress);

                var clientDescription = {
                        clientType: "VNC",
                        requests: {"config":true,"version":false,"time":false,"console":false},
                        receivesWindowModification: true,
                        receivesInputEvents: true,
			sendsPointerData: false,
			sendsMediaStreamFrames: true,
			requestsServerFiles: false,
			sendsWebContentToLoad: true,
			launchesWebBrowser: false,
			sendsVideoSynchonization: false,
			sharesContentWithRemoteServer: false,
			receivesDisplayConfiguration: true,
			receivesClockTime: false,
			requiresFullApps: false,
			requiresAppPositionSizeTypeOnly: false,
			receivesMediaStreamFrames: false,
			receivesPointerData: true,
			receivesRemoteServerInfo: false

                };

		ws.emit('addClient', clientDescription);

		ws.on('frameRate', function(wsio,data) {
                        // how many frames to send before dropping a frame
                        // (invert the fraction of frames dropped to frames sent to display on server)
			console.log("frames sent ", data.frames, ", server dropped ", data.frames - data.displayed);
			//if (data.dropped===0) {
			//    showNth = 1;
        		//    console.log("send 1 every ",showNth, "frames");
			//}
			//else if (data.dropped>20 || displayFrameRate<5) {
			//  showNth = 10;
        		//  console.log("send 1 every ",showNth, "frames");
			//}
			//else if (data.dropped>10 && displayFrameRate<10) {
			//  showNth = 2;
        		//  console.log("send 1 every ",showNth, "frames");
			//}
			//else {
			  showNth = 1;
        		  console.log("send 1 every ",showNth, "frames");
			//}
		});

		ws.on('initialize', function(wsio,data) {
			console.log("initialize:", JSON.stringify(data));
			client.id = data.UID+"|1"; 
			console.log("VNC application id: "+client.id);
			ws.emit('startNewMediaStream',
				{id:client.id,
				src:"R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==",
				type: "image/gif",
				encoding: "base64",
				title:"vnc "+program.server.toString()+":"+(program.port-5900).toString()+" "+program.title,
				width:client.conn.width,
				height:client.conn.height,
				pos: program.ppos,
				resize: program.presize
				}
			);

		});

		ws.on('createAppWindowPositionSizeOnly', function(wsio,data) {
			console.log("createAppWindowPositionSizeOnly: ", JSON.stringify(data));
		});
		
		ws.on('requestNextFrame', function(wsio,data) {
			//console.log(new Date().getTime(), "sage2 requested frame (requestNextFrame)");
			ready = true;
			//client.pointerEvent(ptrx*client.conn.width/elemWidth, ptry*client.conn.height/elemHeight, 0);
		});

		ws.on('eventInItem', function(wsio,evt) {
			//console.log("event: "+JSON.stringify(evt));
                        if (evt.id!=client.id) {
                          //console.log("Rejected event not for this client!?");
                          return;
                        }
			if (evt.type === 'pointerMove') {
				ptrx = evt.position.x;
				ptry = evt.position.y;
				if (evt.data.clipboard !== null && evt.data.clipboard !== undefined) {
					//console.log("clipboard event "+evt.data.clipboard);
					if (evt.data.clipboard !== clipboardText) {
						console.log("update clipboard "+evt.data.clipboard);
						clipboardText = evt.data.clipboard;
						// FIXME: this seems to break server state (?!)
						client.updateClipboard(clipboardText);
						console.log("update clipboard done");
					}
				}
                         	console.log(new Date().getTime(), "pointerMove", evt,client.conn.width,client.conn.height,elemWidth,elemHeight);
				var sx = Math.trunc(evt.position.x*client.conn.width/elemWidth);
				var sy = Math.trunc(evt.position.y*client.conn.height/elemHeight);
				console.log("pointerMove sent: ", sx, sy, JSON.stringify(buttons));
				client.pointerEvent(sx,sy,buttons);
			} else if (evt.type === 'pointerPress') {
				ptrx = evt.position.x;
				ptry = evt.position.y;
                         	console.log("pointerPress", evt);
				var b = evt.data.button;
				if (b === 'left')  buttons = buttons | 1;
				if (b === 'middle')  buttons = buttons | 2;
				if (b === 'right') buttons = buttons | 4;
                                //console.log("buttons ",buttons);
				client.pointerEvent(evt.position.x*client.conn.width/elemWidth,
									evt.position.y*client.conn.height/elemHeight, buttons);
			} else if (evt.type === 'pointerRelease') {
				ptrx = evt.position.x;
				ptry = evt.position.y;
                         	//console.log("pointerRelease", evt);
				var b = evt.data.button;
				if (b === 'left')  buttons = buttons & ~1;
				if (b === 'middle')  buttons = buttons & ~2;
				if (b === 'right') buttons = buttons & ~4;
                                console.log("buttons ",buttons);
				client.pointerEvent(evt.position.x*client.conn.width/elemWidth,
									evt.position.y*client.conn.height/elemHeight, buttons);
			} else if (evt.type === 'keyboard') {
				console.log("event: keyboard:", evt);
				if (keypress) {
                                  code = evt.data.character.charCodeAt();
				  console.log("send keypress ", code);
				  // key down
				  client.keyEvent(code, true);
				  // key up
				  client.keyEvent(code, false);
				  // if user just pressed shift-tab, send shift-key-up event
                                  if (isShift && code==9) {
				    client.keyEvent(65505, false);
				  }
				}
			} else if (evt.type === 'specialKey') {
                                console.log("event: specialKey:", evt);
                                if (evt.data.code == 16) isShift = (evt.data.state=="down");
                                isPressed[evt.data.code] = (evt.data.state==="down");
				//console.log("isPressed ",JSON.stringify(isPressed));
				if (isPressed[16]) {
					console.log("ctrl");
				}
				if (isPressed[16] && isPressed[17] && isPressed[18] && isPressed[68]) {
					console.log("ctrl-alt-shift-d -> cltr-alt-delete");
					// 0xffe3 ctrl
				    	client.keyEvent(0xffe3, true);
					// 0xffe9 alt
				    	client.keyEvent(0xffe9, true);
					// 0xffff delete
				    	client.keyEvent(0xffff, true);
					// 0xffff delete
				    	client.keyEvent(0xffff, false);
					// 0xffe9 alt
				    	client.keyEvent(0xffe9, false);
					// 0xffe3 ctrl
				    	client.keyEvent(0xffe3, false);
				}
                                var result = keysymFromKeyCode(evt.data.code, isShift);
                                console.log("keycode:", result);
				// if the code is deemed "non-special", expect a keypress event (processed elsewhere) and do nothing
				keypress = (result == null);
                                if (!keypress) {
                                  client.keyEvent(result, evt.data.state!=='up');
				  // if user just pressed shift-tab, send shift-key-up event
                                  if (isShift && evt.data.code==9) {
				    //console.log("shift-tab");
				    client.keyEvent(65505, false);
				  }
                                } else {
                                  console.log("deemed non-special (keypress will be used): ", result);
                                }
			} else if (evt.type === 'pointerScroll') {
				console.log("pointerScroll:", evt);
				if (evt.data.wheelDelta<0) {
				  client.pointerEvent(evt.position.x*client.conn.width/elemWidth, evt.position.y*client.conn.height/elemHeight, buttons | 8);
				  client.pointerEvent(evt.position.x*client.conn.width/elemWidth, evt.position.y*client.conn.height/elemHeight, buttons);
				} else if (evt.data.wheelDelta>0) {
				  client.pointerEvent(evt.position.y*client.conn.width/elemWidth, evt.position.y*client.conn.height/elemHeight, buttons | 16);
				  client.pointerEvent(evt.position.y*client.conn.width/elemWidth, evt.position.y*client.conn.height/elemHeight, buttons);
				}
			} else {
				console.log("unknown eventInItem:", evt);
			}
		});


		ws.on('setItemPositionAndSize', function(wsio,data) {
                        // console.log("setItemPositionAndSize", data);
                        if (data.elemId!=client.id) {
                          //console.log("setItemPositionAndSize - rejected event not for this client");
                          return;
                        }
			elemWidth  = data.elemWidth;
			elemHeight = data.elemHeight;
		});

		ws.on('finishedResize', function(wsio,data) {
			//console.log("finishedResize");
		});

		ws.on('stopMediaCapture', function(wsio,data) {
			console.log("stopMediaCapture");
			console.log("server-initiated graceful exit");
			var exec = require('child_process').exec;
			var cmd = "ssh "+program.server.toString()+" '/home/sage/script/close-vnc-server "+program.port.toString()+"'"
			console.log("close command: ", cmd);
			exec(cmd, function(error, stdout, stderr) {
				// command output is in stdout
				exitHandler({exit:true});
			});	
			exitHandler({exit:true});
		});

		//catches ctrl+c event
		process.on('SIGINT', exitHandler.bind(null, {exit:true}));
		process.on('SIGTERM', exitHandler.bind(null, {exit:true}));

		ws.onclose(function() {
			console.log("Closed");
                        process.exit(1);
		});

	});
};

var interval = null;

var lastUpdate = Date.now();

function setRegularRefresh() {
  interval = setInterval(function() {
    console.log("Auto-refresh");
    // auto-close this client if the server does not respond after 2 seconds
    if (Date.now() - lastUpdate > 2000) {
      console.log("TIMEOUT - server does not respond?");
      //ws.emit('stopMediaBlockStream', {id: client.id});
      //exit(1);
    }
    // send last update again to server to force a new request (seems to be the only way to unblock things e.g. after triple-click?)
    if (lastBuffer != null) {
      ws.emit('updateMediaStreamFrame',
	lastBuffer
      );
    }
    //client.pointerEvent(ptrx*client.conn.width/elemWidth, ptry*client.conn.height/elemHeight, 0);
  // end of setInterval call
  }, 250);
}

var frameCount = 0;
var showNth = 2;

var lastBuffer = null;


function framecb(image)
{
        //console.log(new Date().getTime(), "VNC server provided new frame");
        // heart beat check server availability
        lastUpdate = Date.now();
        //if (interval===null) { setRegularRefresh(); }
        frameCount = frameCount + 1;
	if (client.id!==null && client.id!==undefined && (ready || (frameCount % 2 == 0))) {
	// LAST VERSION
	//if (client.id!==null && client.id!==undefined && (frameCount % 2 == 0)) {
	//if (frameCount % 2 == 0) {
	//if (true) {
                //console.log('framecb');
                var nulB = new Buffer([0]);
                // id, state-type, state-encoding, state-src
                //var str64 = image.toString('base64');
                var src = image;
                //console.log("src ",src.length);
                //var i;
                //for (i=0; i<10; i++) {
                //  console.log(i, src[i]);
                //}
                var buffer = Buffer.concat([new Buffer(client.id), nulB, new Buffer("image/jpeg"), nulB, new Buffer("Buffer"), nulB, new Buffer(src)]);
                ws.emit('updateMediaStreamFrame',
                        buffer
                );
                //console.log(new Date().getTime(), "sent frame to sage2 (updateMediaStreamFrame");
                 //{
                        //id: client.id,
                        //state: {src: image.toString('binary'),
                        //              type: "image/jpeg",
                        //              encoding: "binary"}
                  //}
                lastBuffer = buffer;
		ready = true;
	} else {
		console.log('drop frame ', frameCount);
        }
}

function clipboardcb(text) {
	console.log("vnc_client.js: clipboard: "+text);
	ws.emit('clipboard',{clipboard: text});
}


client.open( opencb , framecb, clipboardcb );
