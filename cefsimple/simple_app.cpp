// Copyright (c) 2013 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#include "simple_app.h"

#include <string>

#include "util.h"
#include "include/cef_browser.h"
#include "include/cef_command_line.h"


SimpleApp::SimpleApp() {
}


void SimpleApp::OnContextInitialized() {
	REQUIRE_UI_THREAD();

	std::string url;

	// Check if a "--sage=" value was provided via the command-line. If so, use
	// that instead of the default URL.
	CefRefPtr<CefCommandLine> command_line = CefCommandLine::GetGlobalCommandLine();
	url = command_line->GetSwitchValue("sage");
	if (url.empty())
		url = "wss://localhost:443";

	fprintf(stderr, "SAGE websocket> [%s]\n", url.c_str());

	// SimpleHandler implements browser-level callbacks.
	myhandler = new SimpleHandler(1280, 720);
}


