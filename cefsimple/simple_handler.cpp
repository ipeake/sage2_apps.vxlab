// Copyright (c) 2013 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#include "simple_handler.h"

#include <sstream>
#include <string>

#include "util.h"
#include "include/cef_app.h"
#include "include/cef_runnable.h"


#include "websocketIO.h"


// function prototypes
std::vector<std::string> split(std::string s, char delim);
void ws_open(websocketIO* ws);
void ws_initialize(websocketIO* ws, std::string data);
void ws_openWebBrowser(websocketIO* ws, std::string data);
void ws_requestNextFrame(websocketIO* ws, std::string data);
void ws_stopMediaCapture(websocketIO* ws, std::string data);
void ws_setItemPositionAndSize(websocketIO* ws, std::string data);
void ws_finishedResize(websocketIO* ws, std::string data);
void ws_eventInItem(websocketIO* ws, std::string data);
void onPaint(int browserIdx);

// globals
bool continuous_resize;
std::string uniqueID;
websocketIO* wsio;
std::vector< CefRefPtr<CefBrowser> > browserWindows;
std::vector<bool> windowOpen;
std::vector<bool> server_ready;
std::vector<bool> frame_updated;

namespace {

	SimpleHandler* g_instance = NULL;

}  // namespace


SimpleHandler::SimpleHandler(int w, int h) : is_closing_(false) {
	ASSERT(!g_instance);
	g_instance = this;

	// Default width and height for browser windows
	m_width  = w;
	m_height = h;

	continuous_resize = false;

	wsio = new websocketIO();
	wsio->openCallback(ws_open);

	wsio->on("initialize",       ws_initialize);
	wsio->on("openWebBrowser",   ws_openWebBrowser);
	wsio->on("requestNextFrame", ws_requestNextFrame);
	wsio->on("stopMediaCapture", ws_stopMediaCapture);
	wsio->on("finishedResize",   ws_finishedResize);
	wsio->on("eventInItem",      ws_eventInItem);
	wsio->on("setItemPositionAndSize", ws_setItemPositionAndSize);

	std::string ws_uri = "wss://localhost:443";

	wsio->run(ws_uri);
}

SimpleHandler::~SimpleHandler() {
	g_instance = NULL;
}

// static
SimpleHandler* SimpleHandler::GetInstance() {
	return g_instance;
}

void SimpleHandler::OnAfterCreated(CefRefPtr<CefBrowser> browser) {
	REQUIRE_UI_THREAD();

fprintf(stderr, "OnAfterCreated\n");
	// Store widht/height for each browser window
	BrowserInfo binfo;
	binfo.id     = browser->GetIdentifier();
	binfo.width  = m_width;
	binfo.height = m_height;
	browsersInfos.push_back( binfo );

	// Add to the list of existing browsers.
	browser_list_.push_back(browser);

	browserWindows.push_back(browser);
	windowOpen.push_back(true);
	server_ready.push_back(false);
	frame_updated.push_back(false);

	int browserIdx = browser->GetIdentifier() - 1;
	int width  = GetDefaultWidth();
	int height = GetDefaultHeight();

    // send startNewMediaStream message
	boost::property_tree::ptree emit_data;
	emit_data.put<std::string>("id", uniqueID+"|"+boost::to_string(browserIdx));
	emit_data.put<std::string>("title", "SAGE2 Web Browser");
	emit_data.put<std::string>("src", "R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==");
	emit_data.put<std::string>("type", "image/gif");
	emit_data.put<std::string>("encoding", "base64");
	emit_data.put<int>("width",  width);
	emit_data.put<int>("height", height);
	wsio->emit("startNewMediaStream", emit_data);

	fprintf(stderr, "startNewMediaStream\n");
}

bool SimpleHandler::DoClose(CefRefPtr<CefBrowser> browser) {
	REQUIRE_UI_THREAD();

	// Closing the main window requires special handling. See the DoClose()
	// documentation in the CEF header for a detailed destription of this
	// process.
	if (browser_list_.size() == 1) {
    // Set a flag to indicate that the window close should be allowed.
		is_closing_ = true;
	}

	// Allow the close. For windowed browsers this will result in the OS close
	// event being sent.
	return false;
}

void SimpleHandler::OnBeforeClose(CefRefPtr<CefBrowser> browser) {
	REQUIRE_UI_THREAD();

	fprintf(stderr, "OnBeforeClose\n");

	// Remove from the list of existing browsers.
	BrowserList::iterator bit = browser_list_.begin();
	for (; bit != browser_list_.end(); ++bit) {
		if ((*bit)->IsSame(browser)) {
			browser_list_.erase(bit);
			break;
		}
	}

	if (browser_list_.empty()) {
		// All browser windows have closed. Quit the application message loop.
		//CefQuitMessageLoop();
	}
}


void SimpleHandler::SetRenderSize(CefRefPtr<CefBrowser> browser, int width, int height) {
	int id = browser->GetIdentifier();
	browsersInfos[id-1].width  = width;
	browsersInfos[id-1].height = height;
	fprintf(stderr, "SetRenderSize: %d to %dx%d\n", id, width, height);
	browser->GetHost()->WasResized();
}

bool SimpleHandler::GetViewRect(CefRefPtr<CefBrowser> browser, CefRect &rect) {
	rect.x = 0;
	rect.y = 0;
	int id = browser->GetIdentifier();
	rect.width  = browsersInfos[id-1].width;
	rect.height = browsersInfos[id-1].height;
	//fprintf(stderr, "GetViewRect: %d to %dx%d\n", id, rect.width, rect.height);

	return true;
}

void SimpleHandler::OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList &dirtyRects, const void *buffer, int width, int height) {
	// length has size of raw jpeg character array
	//  jpeg is raw jpeg character array
	unsigned long length, base64_length;
	unsigned char* jpeg = BGRA_to_JPEG((unsigned char*)buffer, width, height, 75, &length);
	char* jpeg_base64 = base64_encode(jpeg, length, &base64_length);
	//jpegFrame = std::string(jpeg_base64, base64_length);
    //fprintf(stderr, "OnPaint: jpeg %ld base64 %ld\n", length, base64_length);
	int idx = browser->GetIdentifier();
	browsersInfos[idx-1].jpegFrame = std::string(jpeg_base64, base64_length);

#if 0
    static int count = 0;
    FILE *f;
    char filename[256];
    memset(filename, 0, 256);
    sprintf(filename, "out%03d.jpg", count++);
    f=fopen(filename, "w+");
    fwrite(jpeg, 1, length, f);
    fclose(f);
#endif
    delete [] jpeg;
    delete [] jpeg_base64;

    onPaint(idx - 1);
}


void SimpleHandler::OnLoadError(CefRefPtr<CefBrowser> browser,
	CefRefPtr<CefFrame> frame,
	ErrorCode errorCode,
	const CefString& errorText,
	const CefString& failedUrl) {
	REQUIRE_UI_THREAD();

	// Don't display an error for downloaded files.
	if (errorCode == ERR_ABORTED)
		return;

	// Display a load error message.
	std::stringstream ss;
	ss << "<html><body bgcolor=\"white\">"
		"<h2>Failed to load URL " << std::string(failedUrl) <<
		" with error " << std::string(errorText) << " (" << errorCode <<
		").</h2></body></html>";
	frame->LoadString(ss.str(), failedUrl);
}

void SimpleHandler::CloseAllBrowsers(bool force_close) {
	fprintf(stderr, "CloseAllBrowsers\n");
	if (!CefCurrentlyOn(TID_UI)) {
		// Execute on the UI thread.
		CefPostTask(TID_UI,
			NewCefRunnableMethod(this, &SimpleHandler::CloseAllBrowsers,
				force_close));
		return;
	}

	if (browser_list_.empty())
		return;

	BrowserList::const_iterator it = browser_list_.begin();
	for (; it != browser_list_.end(); ++it)
		(*it)->GetHost()->CloseBrowser(force_close);
}



/******** Helper Functions ********/
std::vector<std::string> split(std::string s, char delim) {
	std::stringstream ss(s);
	std::string item;
	std::vector<std::string> elems;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}

/******** WebSocket Callback Functions ********/
void ws_open(websocketIO* ws) {
	printf("WEBSOCKET OPEN\n");

    // send addClient message
	boost::property_tree::ptree data;
	data.put<std::string>("clientType", "webBrowser");
	data.put<bool>("sendsMediaStreamFrames", true);
	data.put<bool>("receivesWindowModification", true);
	data.put<bool>("receivesInputEvents", true);

	ws->emit("addClient", data);
}

void ws_initialize(websocketIO* ws, std::string data) {
	boost::property_tree::ptree json_data;
	std::istringstream iss(data);
	boost::property_tree::read_json(iss, json_data);

	uniqueID = json_data.get<std::string> ("data.UID");
	printf("ID: %s\n", uniqueID.c_str());
}

void ws_openWebBrowser(websocketIO* ws, std::string data) {
	REQUIRE_UI_THREAD();

	boost::property_tree::ptree json_data;
	std::istringstream iss(data);
	boost::property_tree::read_json(iss, json_data);

	std::string wb_url = json_data.get<std::string> ("data.url");

	printf("\nOPEN WEB BROWSER: %s\n\n", wb_url.c_str());

    // Information used when creating the native window.
	CefWindowInfo window_info;
    // Specify CEF browser settings here.
	CefBrowserSettings browser_settings;

#if defined(OS_WIN)
    // On Windows we need to specify certain flags that will be passed to
    // CreateWindowEx().
	window_info.SetAsPopup(NULL, "cefsimple");
#endif

	window_info.SetAsOffScreen(NULL);

    // Create the first browser window.
	CefBrowserHost::CreateBrowser(window_info, SimpleHandler::GetInstance(), wb_url, browser_settings, NULL);
}

void ws_requestNextFrame(websocketIO* ws, std::string data) {
	boost::property_tree::ptree json_data;
	std::istringstream iss(data);
	boost::property_tree::read_json(iss, json_data);

	std::string streamId = json_data.get<std::string> ("data.streamId");
	int idx = atoi(streamId.c_str());

	//fprintf(stderr, "Here1: %d\n", idx);

	if(windowOpen[idx]) { // if browser window is still open
		server_ready[idx] = true;

		//fprintf(stderr, "Here2: %d\n", idx);

		if(frame_updated[idx]) {
			frame_updated[idx] = false;
			server_ready[idx] = false;

			fprintf(stderr, "Here3 ws_requestNextFrame\n");

       		CefRefPtr<CefBrowser> browser = browserWindows[idx];
			std::string jpegFrame = SimpleHandler::GetInstance()->GetJPEGFrame(browser);

			boost::property_tree::ptree state;
			state.put<std::string>("src", jpegFrame);
			state.put<std::string>("type", "image/jpeg");
			state.put<std::string>("encoding", "base64");
			boost::property_tree::ptree emit_data;
			emit_data.put<std::string>("id", uniqueID+"|"+boost::to_string(idx));
			emit_data.put_child("state", state);
			ws->emit("updateMediaStreamFrame", emit_data);
		}
	}
}

void ws_stopMediaCapture(websocketIO* ws, std::string data) {
	fprintf(stderr, "stopMediaCapture\n");
	boost::property_tree::ptree json_data;
	std::istringstream iss(data);
	boost::property_tree::read_json(iss, json_data);

	std::string streamId = json_data.get<std::string> ("data.streamId");
	int idx = atoi(streamId.c_str());

	printf("STOP MEDIA CAPTURE: %d\n", idx);

	CefRefPtr<CefBrowser> browser = browserWindows[idx];
	browser->GetHost()->CloseBrowser(false);
	windowOpen[idx] = false;
}

void ws_setItemPositionAndSize(websocketIO* ws, std::string data) {
    // update browser window size during resize
	if(continuous_resize){
		boost::property_tree::ptree json_data;
		std::istringstream iss(data);
		boost::property_tree::read_json(iss, json_data);

		std::string id = json_data.get<std::string> ("data.elemId");
		std::vector<std::string> elemData = split(id, '|');
		std::string uid = "";
		int idx = -1;
		if(elemData.size() == 2){
			uid = elemData[0];
			idx = atoi(elemData[1].c_str());
		}

		if(uid == uniqueID) {
            if(idx < (int)windowOpen.size() && windowOpen[idx]) { // if borwser window is still open
            	std::string w = json_data.get<std::string> ("data.elemWidth");
            	std::string h = json_data.get<std::string> ("data.elemHeight");

            	float width  = atof(w.c_str());
            	float height = atof(h.c_str());

                CefRefPtr<CefBrowser> browser = browserWindows[idx];
                SimpleHandler::GetInstance()->SetRenderSize(browser, (int)width, (int)height);
            }
        }
    }
}

void ws_finishedResize(websocketIO* ws, std::string data) {
	boost::property_tree::ptree json_data;
	std::istringstream iss(data);
	boost::property_tree::read_json(iss, json_data);

	fprintf(stderr, "ws_finishedResize\n");

	std::string id = json_data.get<std::string> ("data.id");
	std::vector<std::string> elemData = split(id, '|');
	std::string uid = "";
	int idx = -1;
	if(elemData.size() == 2){
		uid = elemData[0];
		idx = atoi(elemData[1].c_str());
	}

    // if(uid == uniqueID) {
    //     if(idx < (int)windowOpen.size() && windowOpen[idx]) { // if borwser window is still open
	std::string w = json_data.get<std::string> ("data.elemWidth");
	std::string h = json_data.get<std::string> ("data.elemHeight");

	float width  = atof(w.c_str());
	float height = atof(h.c_str());

	fprintf(stderr, "resizing %d to %.1f %.1f \n", idx, width, height);

    CefRefPtr<CefBrowser> browser = browserWindows[idx];

	//fprintf(stderr, "done %d\n", browser->GetIdentifier());

	SimpleHandler::GetInstance()->SetRenderSize(browser, (int)width, (int)height);
            // OSRenderHandler* osrHandler = browserWindows[idx]->GetOSRenderHandler();
            // CefRefPtr<CefBrowser> browser = browserWindows[idx]->GetBrower();
            // if(browser != NULL) {
            //     fprintf(stderr, "browser not null %d %d\n", (int)width, (int)height);
            //     osrHandler->SetRenderSize(browser, (int)width, (int)height);
            // }
            // else {
            //     fprintf(stderr, "browser null %d %d\n", (int)width, (int)height);
            //     browserWindows[idx]->SetInitialRenderSize((int)width, (int)height);
            // }
    //     }
    // }
}

// {id: elem.id, type: "pointerPress",
//  position: ePosition, user: eUser, data: data, date: now};

void ws_eventInItem(websocketIO* ws, std::string data) {
	boost::property_tree::ptree json_data;
	std::istringstream iss(data);
	boost::property_tree::read_json(iss, json_data);

	std::string id = json_data.get<std::string> ("data.id");
	std::vector<std::string> elemData = split(id, '|');
	std::string uid = "";
	int idx = -1;
	if(elemData.size() == 2){
		uid = elemData[0];
		idx = atoi(elemData[1].c_str());
	}

	if(uid == uniqueID) {
        if(windowOpen[idx]) { // if borwser window is still open
            // OSRenderHandler* osrHandler = browserWindows[idx]->GetOSRenderHandler();
            // CefRefPtr<CefBrowser> browser = browserWindows[idx]->GetBrower();
			CefRefPtr<CefBrowser> browser = browserWindows[idx];

        	std::string eventType = json_data.get<std::string> ("data.type");

        	if(eventType == "pointerMove") {
        		std::string x = json_data.get<std::string> ("data.position.x");
        		std::string y = json_data.get<std::string> ("data.position.y");

        		int mouseX = atoi(x.c_str());
        		int mouseY = atoi(y.c_str());

                SimpleHandler::GetInstance()->PointerMove(browser, mouseX, mouseY);
        	}
        	else if(eventType == "pointerPress") {
        		std::string x = json_data.get<std::string> ("data.position.x");
        		std::string y = json_data.get<std::string> ("data.position.y");
        		std::string button = json_data.get<std::string> ("data.data.button");

        		int mouseX = atoi(x.c_str());
        		int mouseY = atoi(y.c_str());

                SimpleHandler::GetInstance()->PointerPress(browser, mouseX, mouseY, button);
        	}
        	else if(eventType == "pointerRelease") {
        		std::string x = json_data.get<std::string> ("data.position.x");
        		std::string y = json_data.get<std::string> ("data.position.y");
        		std::string button = json_data.get<std::string> ("data.data.button");

        		int mouseX = atoi(x.c_str());
        		int mouseY = atoi(y.c_str());

                SimpleHandler::GetInstance()->PointerRelease(browser, mouseX, mouseY, button);
        	}
        	else if(eventType == "pointerScroll") {
        		std::string x = json_data.get<std::string> ("data.position.x");
        		std::string y = json_data.get<std::string> ("data.position.y");
        		std::string wheelDelta = json_data.get<std::string> ("data.data.scale");

        		fprintf(stderr, "Wheel %s\n", wheelDelta.c_str());
        		int mouseX = atoi(x.c_str());
        		int mouseY = atoi(y.c_str());

        		float deltaY = atof(wheelDelta.c_str());
        		if (deltaY>1) deltaY = 1;
        		else if (deltaY<1) deltaY = -1;

                SimpleHandler::GetInstance()->PointerScroll(browser, mouseX, mouseY, 0, (int)deltaY);
        	}
        	else if(eventType == "specialKey") {
        		std::string code  = json_data.get<std::string> ("data.data.code");
        		std::string state = json_data.get<std::string> ("data.data.state");

        		int charCode = atoi(code.c_str());

        		SimpleHandler::GetInstance()->SpecialKey(browser, charCode, state);
        	}
        	else if(eventType == "keyboard") {
				std::string code = json_data.get<std::string> ("data.data.character");

				fprintf(stderr, "keyboard event: [%s]\n", code.c_str());

				int charCode = atoi(code.c_str());
				SimpleHandler::GetInstance()->Keyboard(browser, charCode);
        	}
        }
    }
}

void onPaint(int idx) {
	//fprintf(stderr, "onPaint: %d\n", idx);
    if(windowOpen[idx]) { // if browser window is still open
        CefRefPtr<CefBrowser> browser = browserWindows[idx];
		std::string jpegFrame = SimpleHandler::GetInstance()->GetJPEGFrame(browser);

    	frame_updated[idx] = true;
    	if (server_ready[idx]) {
    		frame_updated[idx] = false;
    		server_ready[idx] = false;

			fprintf(stderr, "Sending in onPaint\n");

			boost::property_tree::ptree state;
			state.put<std::string>("src", jpegFrame);
			state.put<std::string>("type", "image/jpeg");
			state.put<std::string>("encoding", "base64");
			boost::property_tree::ptree emit_data;
			emit_data.put<std::string>("id", uniqueID+"|"+boost::to_string(idx));
			emit_data.put_child("state", state);
			wsio->emit("updateMediaStreamFrame", emit_data);
    	}
    }
}


/******** Auxiliary App Functions ********/

void AppQuitMessageLoop() {
	CefQuitMessageLoop();
}





std::string SimpleHandler::GetJPEGFrame(CefRefPtr<CefBrowser> browser) {
	int id = browser->GetIdentifier();
	return browsersInfos[id-1].jpegFrame;
}

void SimpleHandler::PointerMove(CefRefPtr<CefBrowser> browser, int mouseX, int mouseY) {
	CefMouseEvent mouse_event;
	mouse_event.x = mouseX;
	mouse_event.y = mouseY;
	mouse_event.modifiers = 0;

	fprintf(stderr, "Pointer MOVE\n");
	browser->GetHost()->SendMouseMoveEvent(mouse_event, false);
}

void SimpleHandler::PointerPress(CefRefPtr<CefBrowser> browser, int mouseX, int mouseY, std::string button) {
	CefMouseEvent mouse_event;
	mouse_event.x = mouseX;
	mouse_event.y = mouseY;
	mouse_event.modifiers = 0;

	CefBrowserHost::MouseButtonType btn;
	if (button == "left")  btn = MBT_LEFT;
	if (button == "right") btn = MBT_RIGHT;

	fprintf(stderr, "Pointer PRESS\n");

	browser->GetHost()->SendMouseClickEvent(mouse_event, btn, false, 1);
	browser->GetHost()->SendFocusEvent(true);
}

void SimpleHandler::PointerRelease(CefRefPtr<CefBrowser> browser, int mouseX, int mouseY, std::string button) {
	CefMouseEvent mouse_event;
	mouse_event.x = mouseX;
	mouse_event.y = mouseY;
	mouse_event.modifiers = 0;

	CefBrowserHost::MouseButtonType btn;
	if (button == "left")  btn = MBT_LEFT;
	if (button == "right") btn = MBT_RIGHT;

	fprintf(stderr, "Pointer RELEASED\n");

	browser->GetHost()->SendMouseClickEvent(mouse_event, btn, true, 1);
}

void SimpleHandler::PointerScroll(CefRefPtr<CefBrowser> browser, int mouseX, int mouseY, int deltaX, int deltaY) {
	CefMouseEvent mouse_event;
	mouse_event.x = mouseX;
	mouse_event.y = mouseY;
	mouse_event.modifiers = 0;

	fprintf(stderr, "Pointer SCROLL: %d %d\n", deltaX, deltaY);

	browser->GetHost()->SendMouseWheelEvent(mouse_event, deltaX, deltaY);
}

void SimpleHandler::SpecialKey(CefRefPtr<CefBrowser>browser, int charCode, std::string state) {
	unsigned int nativeCode  = JavaScriptCodeToNativeCode(charCode);
    //unsigned short unmodCode = JavaScriptCodeToUnmodifiedCharCode(charCode);

	if(state == "down") {
		CefKeyEvent keyEvent;
        keyEvent.character = 0; // unmodCode;
        keyEvent.unmodified_character = 0; // unmodCode;
        keyEvent.native_key_code = nativeCode;
        keyEvent.modifiers = 0;
        keyEvent.type = KEYEVENT_KEYDOWN;
        
        printf("KeyDown: %d (from charCode %d)\n", nativeCode, charCode);
        
        browser->GetHost()->SendKeyEvent(keyEvent);
    }
    else if(state == "up") {
    	CefKeyEvent keyEvent;
        keyEvent.character = 0; // unmodCode;
        keyEvent.unmodified_character = 0; // unmodCode;
        keyEvent.native_key_code = nativeCode;
        keyEvent.modifiers = 0;
        keyEvent.type = KEYEVENT_KEYUP;
        
        printf("KeyUp: %d (from charCode %d)\n", nativeCode, charCode);
        
       	browser->GetHost()->SendKeyEvent(keyEvent);
    }
}


void SimpleHandler::Keyboard(CefRefPtr<CefBrowser>browser, int charCode) {
	CefKeyEvent keyEvent;
	keyEvent.character = charCode;
	keyEvent.type = KEYEVENT_CHAR;

	printf("Keyboard: %d\n", charCode);

	browser->GetHost()->SendKeyEvent(keyEvent);
}


unsigned char* SimpleHandler::BGRA_to_RGB(unsigned char* bgra, int width, int height) {
	unsigned char* rgb = new unsigned char[width*height*3];

	for(int x=0; x<width*height; x++) {
		rgb[3*x + 0] = bgra[4*x + 2];
		rgb[3*x + 1] = bgra[4*x + 1];
		rgb[3*x + 2] = bgra[4*x + 0];
	}

	return rgb;
}

unsigned char* SimpleHandler::BGRA_to_JPEG(unsigned char* bgra, int width, int height, int quality, unsigned long* arr_length) {
	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;

	JSAMPROW row_pointer[1];
	int row_stride;

	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_compress(&cinfo);

	unsigned char *jpeg_buffer_raw = NULL;
	unsigned long outbuffer_size = 0;
	jpeg_mem_dest(&cinfo, &jpeg_buffer_raw, &outbuffer_size);

	cinfo.image_width = width;
	cinfo.image_height = height;
	cinfo.input_components = 3;
	cinfo.in_color_space = JCS_RGB;

	jpeg_set_defaults(&cinfo);
	jpeg_set_quality(&cinfo, quality, TRUE);
	jpeg_start_compress(&cinfo, TRUE);

	row_stride = width * 4;

	while(cinfo.next_scanline < cinfo.image_height) {
		unsigned char* rgb = BGRA_to_RGB(&bgra[cinfo.next_scanline * row_stride], width, 1);
		row_pointer[0] = (JSAMPROW) rgb;
		jpeg_write_scanlines(&cinfo, row_pointer, 1);
		delete [] rgb;
	}

	jpeg_finish_compress(&cinfo);
	jpeg_destroy_compress(&cinfo);

	*arr_length = outbuffer_size;

	return jpeg_buffer_raw;
}

char* SimpleHandler::base64_encode(unsigned char* bin, unsigned long input_length, unsigned long *output_length) {
	*output_length = 4 * ((input_length + 2) / 3);

	char*encoded_data = new char[*output_length];
	if (encoded_data == NULL) return NULL;

	const char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
	'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
	'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	'w', 'x', 'y', 'z', '0', '1', '2', '3',
	'4', '5', '6', '7', '8', '9', '+', '/'};
	const int mod_table[] = {0, 2, 1};

	unsigned long i=0, j=0;
	while (i<input_length) {
		uint32_t octet_a = i < input_length ? bin[i++] : 0;
		uint32_t octet_b = i < input_length ? bin[i++] : 0;
		uint32_t octet_c = i < input_length ? bin[i++] : 0;

		uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

		encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
		encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
		encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
		encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
	}

	for (int i=0; i<mod_table[input_length % 3]; i++)
		encoded_data[*output_length - 1 - i] = '=';

	return encoded_data;
}



unsigned int SimpleHandler::JavaScriptCodeToNativeCode(unsigned short JSCode) {
	switch (JSCode) {
		case 8:
		return XK_BackSpace;
		case 9:
		return XK_Tab;
		case 13:
		return XK_Return;
		case 16:
		return XK_Shift_L;
		case 17:
		return XK_Control_L;
		case 18:
		return XK_Alt_L;
		case 20:
		return XK_Caps_Lock;
		case 27:
		return XK_Escape;
		case 32:
		return XK_space;
		case 33:
		return XK_Page_Up;
		case 34:
		return XK_Page_Down;
		case 35:
		return XK_End;
		case 36:
		return XK_Home;
		case 37:
		return XK_Left;
		case 38:
		return XK_Up;
		case 39:
		return XK_Right;
		case 40:
		return XK_Down;
		case 46:
		return XK_Delete;
		case 48:
		return XK_0;
		case 49:
		return XK_1;
		case 50:
		return XK_2;
		case 51:
		return XK_3;
		case 52:
		return XK_4;
		case 53:
		return XK_5;
		case 54:
		return XK_6;
		case 55:
		return XK_7;
		case 56:
		return XK_8;
		case 57:
		return XK_9;
		case 65:
		return XK_A;
		case 66:
		return XK_B;
		case 67:
		return XK_C;
		case 68:
		return XK_D;
		case 69:
		return XK_E;
		case 70:
		return XK_F;
		case 71:
		return XK_G;
		case 72:
		return XK_H;
		case 73:
		return XK_I;
		case 74:
		return XK_J;
		case 75:
		return XK_K;
		case 76:
		return XK_L;
		case 77:
		return XK_M;
		case 78:
		return XK_N;
		case 79:
		return XK_O;
		case 80:
		return XK_P;
		case 81:
		return XK_Q;
		case 82:
		return XK_R;
		case 83:
		return XK_S;
		case 84:
		return XK_T;
		case 85:
		return XK_U;
		case 86:
		return XK_V;
		case 87:
		return XK_W;
		case 88:
		return XK_X;
		case 89:
		return XK_Y;
		case 90:
		return XK_Z;
		case 91:
		return XK_Meta_L; // Apple Command Key ?
		case 93:
		return XK_Meta_L;
		case 96:
		return XK_KP_0;
		case 97:
		return XK_KP_1;
		case 98:
		return XK_KP_2;
		case 99:
		return XK_KP_3;
		case 100:
		return XK_KP_4;
		case 101:
		return XK_KP_5;
		case 102:
		return XK_KP_6;
		case 103:
		return XK_KP_7;
		case 104:
		return XK_KP_8;
		case 105:
		return XK_KP_9;
		case 106:
		return XK_KP_Multiply;
		case 107:
		return XK_KP_Add;
		case 109:
		return XK_KP_Subtract;
		case 110:
		return XK_KP_Decimal;
		case 111:
		return XK_KP_Divide;
		case 112:
		return XK_F1;
		case 113:
		return XK_F2;
		case 114:
		return XK_F3;
		case 115:
		return XK_F4;
		case 116:
		return XK_F5;
		case 117:
		return XK_F6;
		case 118:
		return XK_F7;
		case 119:
		return XK_F8;
		case 120:
		return XK_F9;
		case 121:
		return XK_F10;
		case 122:
		return XK_F11;
		case 123:
		return XK_F12;
		case 186:
		return XK_semicolon;
		case 187:
		return XK_equal;
		case 188:
		return XK_comma;
		case 189:
		return XK_minus;
		case 190:
		return XK_period;
		case 191:
		return XK_slash;
		case 192:
		return XK_grave;
		case 219:
		return XK_bracketleft;
		case 220:
		return XK_backslash;
		case 221:
		return XK_bracketright;
		case 222:
		return XK_quoteright;
		default:
		return 0;
	}
	return 0;
}

unsigned short SimpleHandler::JavaScriptCodeToUnmodifiedCharCode(unsigned short JSCode) {
	switch (JSCode) {
		case 8:
		return XK_BackSpace;
		case 9:
		return XK_Tab;
		case 13:
		return XK_Return;
		case 16:
		return 0;
		case 17:
		return 0;
		case 18:
		return 0;
		case 20:
		return 0;
		case 27:
		return XK_Escape;
		case 32:
		return XK_space;
		case 33:
		return 65535; // NSPageUpFunctionKey
		case 34:
		return 65535; // NSPageDownFunctionKey
		case 35:
		return 65535; // NSEndFunctionKey
		case 36:
		return 65535; // NSHomeFunctionKey
		case 37:
		return 65535; // NSLeftArrowFunctionKey
		case 38:
		return 65535; // NSUpArrowFunctionKey
		case 39:
		return 65535; // NSRightArrowFunctionKey
		case 40:
		return 65535; // NSDownArrowFunctionKey
		case 46:
		return XK_Delete;
		case 48:
		return '0';
		case 49:
		return '1';
		case 50:
		return '2';
		case 51:
		return '3';
		case 52:
		return '4';
		case 53:
		return '5';
		case 54:
		return '6';
		case 55:
		return '7';
		case 56:
		return '8';
		case 57:
		return '9';
		case 65:
		return 'a';
		case 66:
		return 'b';
		case 67:
		return 'c';
		case 68:
		return 'd';
		case 69:
		return 'e';
		case 70:
		return 'f';
		case 71:
		return 'g';
		case 72:
		return 'h';
		case 73:
		return 'i';
		case 74:
		return 'j';
		case 75:
		return 'k';
		case 76:
		return 'l';
		case 77:
		return 'm';
		case 78:
		return 'n';
		case 79:
		return 'o';
		case 80:
		return 'p';
		case 81:
		return 'q';
		case 82:
		return 'r';
		case 83:
		return 's';
		case 84:
		return 't';
		case 85:
		return 'u';
		case 86:
		return 'v';
		case 87:
		return 'w';
		case 88:
		return 'x';
		case 89:
		return 'y';
		case 90:
		return 'z';
		case 91:
		return 0;
		case 93:
		return 0;
		case 96:
		return '0';
		case 97:
		return '1';
		case 98:
		return '2';
		case 99:
		return '3';
		case 100:
		return '4';
		case 101:
		return '5';
		case 102:
		return '6';
		case 103:
		return '7';
		case 104:
		return '8';
		case 105:
		return '9';
		case 106:
		return '*';
		case 107:
		return '+';
		case 109:
		return '-';
		case 110:
		return '.';
		case 111:
		return '/';
		case 112:
		return 65535; // NSF1FunctionKey
		case 113:
		return 65535; // NSF2FunctionKey
		case 114:
		return 65535; // NSF3FunctionKey
		case 115:
		return 65535; // NSF4FunctionKey
		case 116:
		return 65535; // NSF5FunctionKey
		case 117:
		return 65535; // NSF6FunctionKey
		case 118:
		return 65535; // NSF7FunctionKey
		case 119:
		return 65535; // NSF8FunctionKey
		case 120:
		return 65535; // NSF9FunctionKey
		case 121:
		return 65535; // NSF10FunctionKey
		case 122:
		return 65535; // NSF11FunctionKey
		case 123:
		return 65535; // NSF12FunctionKey
		case 186:
		return ';';
		case 187:
		return '=';
		case 188:
		return ',';
		case 189:
		return '-';
		case 190:
		return '.';
		case 191:
		return '/';
		case 192:
		return '`';
		case 219:
		return '[';
		case 220:
		return '\\';
		case 221:
		return ']';
		case 222:
		return '\'';
		default:
		return 0;
	}
	return 0;
}
